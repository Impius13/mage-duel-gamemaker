/// scr_cast(combo, cost)
// Cast the spell and clear combo variable

var combo = argument0;

var name = scr_get_name(combo);
var manaCheck = noone;
// Check some spell's special conditions
if (name == "shield") {
    if ((shield == true) or (state == "lit"))
        manaCheck = false;
}

if (manaCheck != false) {
    // Get the mana cost of spell and check if will be cast
    var cost = scr_get_cost(combo);
    manaCheck = scr_subtract_mana(cost);
}

if (manaCheck) {
    // Get the name of spell and cast it
    switch(name) {
        case "fireball": 
            instance_create(x, y, obj_fireball);
            break;
        
        case "frostbolt":
            instance_create(x, y, obj_frostbolt);
            break;
            
        case "lightningbolt":
            instance_create(x, y, obj_lightningbolt);
            break;
            
        case "platform_spike":
            scr_spike_trap();
            break;
            
        case "shield":
            instance_create(x, y, obj_shield);    
            break;
    }
}
