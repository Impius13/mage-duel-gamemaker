/// Get player's input

if (zone == 1) {
    right = keyboard_check(ord("D")) || gamepad_button_check(0, gp_padr);
    left = -(keyboard_check(ord("A")) || gamepad_button_check(0, gp_padl));
    down = keyboard_check(ord("S")) || gamepad_button_check(0, gp_padd);
    
    jump = keyboard_check_pressed(ord("W")) || gamepad_button_check_pressed(0, gp_padu);
    jumpRel = keyboard_check_released(ord("W")) || gamepad_button_check_released(0, gp_padu);
    
    // Movement button checks for the combo system
    rightComb = keyboard_check_pressed(ord("D")) || gamepad_button_check_pressed(0, gp_padr);
    leftComb = keyboard_check_pressed(ord("A")) || gamepad_button_check_pressed(0, gp_padl);
    downComb = keyboard_check_pressed(ord("S")) || gamepad_button_check_pressed(0, gp_padd);
    jumpComb = keyboard_check_pressed(ord("W")) || gamepad_button_check_pressed(0, gp_padu);
    
    button1 = keyboard_check_pressed(ord("G")) || gamepad_button_check_pressed(0, gp_face1);
    button2 = keyboard_check_pressed(ord("H")) || gamepad_button_check_pressed(0, gp_face2);
    button3 = keyboard_check_pressed(ord("J")) || gamepad_button_check_pressed(0, gp_face3);
    button4 = keyboard_check_pressed(ord("K")) || gamepad_button_check_pressed(0, gp_face4);
}

else if (zone == 2) {
    right = keyboard_check(vk_right) || gamepad_button_check(1, gp_padr);
    left = -(keyboard_check(vk_left) || gamepad_button_check(1, gp_padl));
    down = keyboard_check(vk_down) || gamepad_button_check(1, gp_padd);
    
    jump = keyboard_check_pressed(vk_up) || gamepad_button_check_pressed(1, gp_padu);
    jumpRel = keyboard_check_released(vk_up) || gamepad_button_check_released(1, gp_padu);
    
    // Movement button checks for the combo system
    rightComb = keyboard_check_pressed(vk_right) || gamepad_button_check_pressed(1, gp_padr);
    leftComb = keyboard_check_pressed(vk_left) || gamepad_button_check_pressed(1, gp_padl);
    downComb = keyboard_check_pressed(vk_down) || gamepad_button_check_pressed(1, gp_padd);
    jumpComb = keyboard_check_pressed(vk_up) || gamepad_button_check_pressed(1, gp_padu);
    
    button1 = keyboard_check_pressed(vk_numpad7) || gamepad_button_check_pressed(1, gp_face1);
    button2 = keyboard_check_pressed(vk_numpad8) || gamepad_button_check_pressed(1, gp_face2);
    button3 = keyboard_check_pressed(vk_numpad9) || gamepad_button_check_pressed(1, gp_face3);
    button4 = keyboard_check_pressed(vk_numpad5) || gamepad_button_check_pressed(1, gp_face4);
}
