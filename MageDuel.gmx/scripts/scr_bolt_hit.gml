/// scr_bolt_hit(state)
/// Subtract from hp and randomly determine if it affect the player
var effect = argument0;
if (other.zone != zone) {
    instance_destroy();
    with(other) {
        if (shield == false) {
            if (int64(random_range(0, 6)) == 5)
                state = effect;
        }
        scr_subtract_hp(other.damage);
    }
}
