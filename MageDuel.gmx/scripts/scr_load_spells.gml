// Set combos
/*  Combo system

    Legend:
    button1 = 0
    button2 = 1
    button3 = 2
    button4 = 3
    
    right = 4
    left = 5
    down = 6
    jump = 7  */

// Fireball
global.spells[0, 0] = "01";
global.spells[0, 1] = "fireball";
global.spells[0, 2] = "15";

// Frostbolt
global.spells[1, 0] = "10";
global.spells[1, 1] = "frostbolt";
global.spells[1, 2] = "15";

// Lightningbolt
global.spells[2, 0] = "32";
global.spells[2, 1] = "lightningbolt";
global.spells[2, 2] = "15";

// Platform spike
global.spells[3, 0] = "231";
global.spells[3, 1] = "platform_spike";
global.spells[3, 2] = "30";

// Shield
global.spells[4, 0] = "03";
global.spells[4, 1] = "shield";
global.spells[4, 2] = "20";
