// Combo system
/*  Legend:
    button1 = 0
    button2 = 1
    button3 = 2
    button4 = 3
    
    right = 4
    left = 5
    down = 6
    jump = 7  */

// Save pressed button
var button = noone;

if (button1)
    button = 0;
    
else if (button2)
    button = 1;
    
else if (button3)
    button = 2;
    
else if (button4)
    button = 3;
    
else if (rightComb)
    button = 4;
    
else if (leftComb)
    button = 5;

else if (downComb)
    button = 6;
    
else if (jumpComb)
    button = 7;

// Add button number to combo
if (button >= 0) {
    combo += string(button);
    alarm[4] = room_speed / 2;
    
    // If there were found possible combos after the first checking
    if (posLength >= 1) {
        for (var i = 0; i < posLength; i++) {
            for (var j = 0; j < string_length(combo); j++) {
                if (string_char_at(posCombos[i], j + 1) != string_char_at(combo, j + 1)) {
                    scr_combo_remove(i);
                    break;
                }
            }
        }
        
        if (posLength <= 1) {
            // No match was found
            if (posLength == 0) {
                show_debug_message("Clearing combo variable");
                scr_combo_clear();
            }
               
            // Combo is not completed yet
            else if (posCombos[posLength - 1] != combo) {
                ;
            }
            
            // Match found
            else {
                show_debug_message("Combo: " + combo);
                scr_cast(combo);
                scr_combo_clear();
            }
        }
    }
    
    // Check the combo database to store possible matches
    else {
        for (var i = 0; i < array_height_2d(global.spells); i++) {
            if (string_char_at(global.spells[i, 0], 1) == string_char_at(combo, 1)) {
                // Store possible combo and it's mana cost to array
                scr_combo_add(global.spells[i, 0]);
            }
        }
        
        // If no match was found, clear the combo variable
        if (posCombos[0] == "") {
            combo = "";
        }
    }
}
