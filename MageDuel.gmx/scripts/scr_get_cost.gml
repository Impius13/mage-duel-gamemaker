/// scr_get_cost(combo)

var combo = argument0;

for (var i = 0; i < array_height_2d(global.spells); i++) {
    if (global.spells[i, 0] == combo)
        return int64(global.spells[i, 2]);
}
