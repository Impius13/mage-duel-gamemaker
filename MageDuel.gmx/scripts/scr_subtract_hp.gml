/// scr_subtract_hp(hp)

if (sprite_index == spr_player_shield) {
    shield = false;
}

else {
    damage = argument[0];
    hp -= damage;
    
    sprite_index = spr_player_hurt;
    alarm[2] = 10;
}
