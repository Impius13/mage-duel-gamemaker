/// scr_subtract_mana(mana)

var cost = argument0;

if ((mana - cost) < 0) {
    return false;
}

else {
    mana -= cost;
    return true;
}
