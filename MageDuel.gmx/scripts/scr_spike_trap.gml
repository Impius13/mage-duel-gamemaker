// Fill the platfrom with spikes
var platform = noone;

with(instance_nearest(x, y + 32, obj_platform)) {
    // Find the oposite platform
    var line = instance_nearest(x, y, obj_line);
    
    if (zone == 1)
        platform = instance_nearest(line.x + (line.x - x), y, obj_platform);
        
    else
        platform = instance_nearest(line.x - (x - line.x), y, obj_platform);
}

with(platform) {
    var psWidth = 16;
    var count = width / psWidth;
    var psX = x - 64;
    
    for (var i = 0; i < count; i++) {
        instance_create(psX, y, obj_platform_spike);
        psX += psWidth;
    }
}
