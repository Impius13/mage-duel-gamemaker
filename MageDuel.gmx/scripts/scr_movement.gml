scr_input();

// Movement
move = left + right;
if (move != 0) {
    if (hsp == spd) {
        if (move > 0)
            hsp = move * spd;
    }
    
    else if (hsp == -spd) {
        if (move < 0)
            hsp = move * spd;
    }
    
    else
        hsp += move * frict;
}

// If player stopped moving, stop with friction
if (move == 0) {
    if (hsp != 0) {
        if (hsp < 0)
            hsp += frict;       
        else if (hsp > 0)
            hsp -= frict;
    }
}

// Gravity
if (vsp < 10) 
    vsp += grav;
    
// Jump control
if (jumpRel and vsp < -4)
    vsp = -4;

// Jumping
if ((place_meeting(x, y+1, obj_ground)) or (place_meeting(x, y+1, obj_platform))) {
    vsp = jump * (-jump_spd);
}

// Collisions
// With middle line
if (place_meeting(x + hsp, y, obj_line)) {
    while(!place_meeting(x + sign(hsp), y, obj_line)) {
        x += sign(hsp);
    }
    hsp = 0;
}

// With side invisible walls
if (place_meeting(x + hsp, y, obj_invis_wall)) {
    while(!place_meeting(x + sign(hsp), y, obj_invis_wall)) {
        x += sign(hsp);
    }
    hsp = 0;
}

x += hsp;

// With ground
if (place_meeting(x, y + vsp, obj_ground)) {
    while(!place_meeting(x, y + sign(vsp), obj_ground)) {
        y += sign(vsp);
    }
    vsp = 0;
}

// With platform
if (place_meeting(x, y + vsp, obj_platform)) {
    while(!place_meeting(x, y + sign(vsp), obj_platform)) {
        y += sign(vsp);
    }
    vsp = 0;
}

y += vsp;
