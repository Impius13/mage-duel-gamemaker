/// scr_shield_destroy(player)
var player = argument0;

player.sprite_index = spr_player;
player.shield = false;
instance_destroy();
